package com.glearning.orders.util;

import static java.util.stream.IntStream.range;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.github.javafaker.Faker;
import com.glearning.orders.dao.OrderJpaRepository;
import com.glearning.orders.dao.RoleJpaRepository;
import com.glearning.orders.dao.UserJpaRepository;
import com.glearning.orders.model.LineItem;
import com.glearning.orders.model.Order;
import com.glearning.orders.model.Role;
import com.glearning.orders.model.User;

@Component
public class BootstrapAppUtil {

	@Autowired
	private OrderJpaRepository repository;
	
	@Autowired
	private UserJpaRepository userRepository;
	
	
	@Autowired
	private RoleJpaRepository roleRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	private Faker faker = new Faker();

	@Value("${app.ordercount}")
	private int orderCount;

	@EventListener(ApplicationReadyEvent.class)
	public void onReady(ApplicationReadyEvent event){
		range(0, orderCount).forEach(index -> {
			LocalDate orderDate = faker
									.date()
									.past(10, TimeUnit.DAYS)
									.toInstant()
									.atZone(ZoneId.systemDefault())
									.toLocalDate();
			Order order = Order.builder()
							.customerName(faker.name().firstName())
							.date(orderDate)
							.price(faker.number().randomDouble(2, 3000, 5000))
							.build();
			
			range(0, faker.number().numberBetween(2, 4)).forEach(val-> {
				LineItem li = LineItem
								.builder()
								.name(faker.commerce().productName())
								.price(faker.number().randomDouble(2, 300, 500))
								.build();
				order.addLineItem(li);
				
			});
			this.repository.save(order);
		});
	}

	@EventListener(ApplicationReadyEvent.class)
	public void insertUsers(ApplicationReadyEvent event) {
		User kiran = new User();
		kiran.setName("kiran");
		kiran.setPassword(this.passwordEncoder.encode("welcome"));

		User vinay = new User();
		vinay.setName("vinay");
		vinay.setPassword(this.passwordEncoder.encode("welcome"));
		
		this.userRepository.save(kiran);
		this.userRepository.save(vinay);
		
		Role userRole = new Role();
		userRole.setRole("ROLE_USER");
		
		Role adminRole = new Role();
		adminRole.setRole("ROLE_ADMIN");
		
		this.roleRepository.save(userRole);
		this.roleRepository.save(adminRole);
		
		
		kiran.addRole(userRole);
		vinay.addRole(userRole);
		vinay.addRole(adminRole);
		
		this.userRepository.save(kiran);
		this.userRepository.save(vinay);
	}
}
