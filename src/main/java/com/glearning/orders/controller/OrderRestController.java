package com.glearning.orders.controller;

import java.util.Map;
import java.util.Set;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.glearning.orders.model.Order;
import com.glearning.orders.service.OrderService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/orders")
@RequiredArgsConstructor
public class OrderRestController {
	
	private final OrderService orderService;
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Order saveOrder(@RequestBody Order order) {
		return this.orderService.saveOrder(order);
	}
	
	@GetMapping
	public Map<String, Object> fetchOrders(
			@RequestParam(name = "page", required = false, defaultValue = "0") int page, 
			@RequestParam(name = "size", required = false, defaultValue = "10") int size, 
			@RequestParam(name = "order", required = false, defaultValue = "asc") String strDirection, 
			@RequestParam(name = "field", required = false, defaultValue = "customerName") String property){
		return this.orderService.fetchOrders(page, size, strDirection, property);
	}
	
	@GetMapping("/{orderId}")
	public Order fetchOrderById(@PathVariable("orderId") long orderId) {
		return this.orderService.fetchOrderById(orderId);
	}
	
	@GetMapping("/price")
	public Set<Order> fetchOrderById(
			@RequestParam(name = "min", required = false, defaultValue = "200") double min, 
			@RequestParam(name = "max", required = false, defaultValue = "400") double max ) {
		return this.orderService.findOrdersByPriceRange(min, max);
	}

	@GetMapping("/name")
	public Set<Order> fetchOrdersByCustomerName(
			@RequestParam(name = "name", required = true) String customerName) {
		return this.orderService.findOrdersByCustomerName(customerName);
	}

	
	/**
	 * This is to update the order
	 * we will implement this in sometime
	 */
	@PutMapping("/{id}")
	public Order updateOrderById(@RequestBody Order order, @PathVariable("id") long id ) {
		//we will implement this later
		return this.orderService.updateOrder(order, id);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteOrderById(@PathVariable long id) {
		this.orderService.deleteOrderById(id);
	}
}
