package com.glearning.orders.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.glearning.orders.model.User;

@Repository
public interface UserJpaRepository extends JpaRepository<User, Integer>{

	Optional<User> findByName(String username);
}
