package com.glearning.orders.dao;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.glearning.orders.model.Order;

@Repository
public interface OrderJpaRepository extends JpaRepository<Order, Long>{
	
	//custom method to find the orders between a price range
	Set<Order> findByPriceBetween(double min, double max);
	Set<Order> findByCustomerNameLike(String customerName);
}
