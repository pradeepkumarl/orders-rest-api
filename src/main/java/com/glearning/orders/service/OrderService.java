package com.glearning.orders.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.glearning.orders.dao.OrderJpaRepository;
import com.glearning.orders.model.Order;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class OrderService {

	private final OrderJpaRepository repository;

	public Order saveOrder(Order order) {
		return this.repository.save(order);
	}
	
	public Map<String, Object> fetchOrders(int page, int size, String strDirection, String property){
		Sort.Direction direction = strDirection.equalsIgnoreCase("asc") ? Sort.Direction.ASC : Sort.Direction.DESC;
		PageRequest pageRequest = PageRequest.of(page, size, direction, property);
		Page<Order> pageResponse = this.repository.findAll(pageRequest);
		//parse the response and extract the data
		int totalPages = pageResponse.getTotalPages();
		int noOfRecords = pageResponse.getNumberOfElements();
		List<Order> data = pageResponse.getContent();
		
		//put all the response content inside a hashmap and return the hashmap
		Map<String, Object> responseMap = new LinkedHashMap<>();
		responseMap.put("pages", totalPages);
		responseMap.put("records", noOfRecords);
		responseMap.put("data", data);
		
		return responseMap;
	}
	
	public Set<Order> findOrdersByPriceRange(double min, double max){
		return this.repository.findByPriceBetween(min, max);
	}
	
	public Set<Order> findOrdersByCustomerName(String name){
		return this.repository.findByCustomerNameLike(name);
	}
	
	public Order fetchOrderById(long orderId) {
		return this.repository.findById(orderId).orElseThrow(() -> new IllegalArgumentException("invalid order id passed"));
	}
	
	public void deleteOrderById(long id) {
		this.repository.deleteById(id);
	}

	public Order updateOrder(Order order, long id) {
		Order existingOrder = this.fetchOrderById(id);
		existingOrder.setCustomerName(order.getCustomerName());
		existingOrder.setDate(order.getDate());
		existingOrder.setLineItems(order.getLineItems());
		this.repository.save(order);
		return order;
	}
}
