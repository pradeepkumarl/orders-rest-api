package com.glearning.orders.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.actuate.health.Status;
import org.springframework.context.annotation.Configuration;

@Configuration
/*
 * Write a custom endpoint to show if the application is health
 */
public class ApplicationHealthConfig implements HealthIndicator{
	
	@Value("${app.value}")
	private int value;
	
	@Override
	public Health health() {
		boolean isApplicationHealth = value == 1 ? true: false;
		if(isApplicationHealth) {
			return Health.status(Status.UP).withDetail("app", "healthy").build();
		}
		return Health.status(Status.DOWN).withDetail("app", "unhealthy").build();
	}

}
