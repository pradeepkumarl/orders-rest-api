package com.glearning.orders.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.glearning.orders.service.DomainUserDetailsService;

@Configuration
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {
	
	//used to configure both authentication and authorization
	@Autowired
	private DomainUserDetailsService userDetailsService;

	@Autowired
	private PasswordEncoder passwordEncoder;
	
	/*
	 * @Bean
       public UserDetailsService userDetailsService() {
          return username -> {
            if ("user".equals(username)) {
                PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
                UserDetails user = User.builder()
                        .username("user")
                        .password(encoder.encode("password"))
                        .roles("USER")
                        .build();
                return user;
            } else {
                throw new UsernameNotFoundException("User not found");
            }
         };
       }
	 */
	
	//authentication
	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth
			.userDetailsService(this.userDetailsService)
			.passwordEncoder(this.passwordEncoder);
		
	}
	
	/*
	 * @Bean
		public HttpSecurity httpSecurity() throws Exception {
		    return 	new HttpSecurity()
		    				.cors().disable()
		    				.csrf().disable()
		    				.headers().frameOptions().disable()
		    		.authorizeRequests()
					.antMatchers("/login", "/h2-console/**", "/h2-console**", "/actuator/**")
						.permitAll()
					.antMatchers(HttpMethod.GET, "/api/orders/**", "/api/orders**")
						.hasAnyRole("USER", "ADMIN")
					.antMatchers(HttpMethod.POST, "/api/orders/**")
						.hasAnyRole("ADMIN")
					.antMatchers(HttpMethod.PUT, "/api/orders/**")
						.hasAnyRole("ADMIN")
					.antMatchers(HttpMethod.DELETE, "/api/orders/**")
						.hasAnyRole("ADMIN")
					.anyRequest()
						.authenticated()
					.and()
						.formLogin()
					.and()
						.httpBasic()
					.and()
					.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		}

	 */
	
	//authorization
	@Override
	public void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity.cors().disable();
		httpSecurity.csrf().disable();
		httpSecurity.headers().frameOptions().disable();
		
		//define the rules which will tell who can access what url's
		httpSecurity
					.authorizeRequests()
					.antMatchers("/login", "/h2-console/**", "/h2-console**", "/actuator/**")
						.permitAll()
					.antMatchers(HttpMethod.GET, "/api/orders/**", "/api/orders**")
						.hasAnyRole("USER", "ADMIN")
					.antMatchers(HttpMethod.POST, "/api/orders/**")
						.hasAnyRole("ADMIN")
					.antMatchers(HttpMethod.PUT, "/api/orders/**")
						.hasAnyRole("ADMIN")
					.antMatchers(HttpMethod.DELETE, "/api/orders/**")
						.hasAnyRole("ADMIN")
					.anyRequest()
						.authenticated()
					.and()
						.formLogin()
					.and()
						.httpBasic()
					.and()
					.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}
}
